# frozen_string_literal: true

module JH
  module VersionCheckHelper
    def source_host_url
      Gitlab::CN_URL
    end

    def source_code_group
      'gitlab-jh'
    end
  end
end
