# frozen_string_literal: true

module JH
  module BillingPlansHelper
    extend ::Gitlab::Utils::Override

    override :use_new_purchase_flow?
    def use_new_purchase_flow?(namespace)
      false
    end

    override :plan_purchase_or_upgrade_url
    def plan_purchase_or_upgrade_url(group, plan)
      ::Gitlab::Saas.commom_purchase_url
    end
  end
end
