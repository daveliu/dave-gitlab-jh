---
stage: 
group: 
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 使用极狐GitLab **(FREE)**

<!--
Get to know the GitLab end-to-end workflow. Configure permissions,
organize your work, create and secure your application, and analyze its performance. Report on team productivity throughout the process.
-->


了解极狐GitLab 端到端工作流程。<!--配置权限、组织您的工作、创建和保护您的应用程序，并分析其性能。 在整个过程中报告团队生产力。-->

<!--
- [Set up your organization](set_up_organization.md)
- [Organize work with projects](../user/project/index.md)
-->
- [计划并跟踪工作](plan_and_track.md)

<!--
- [Build your application](build_your_application.md)
- [Secure your application](../user/application_security/index.md)
- [Release your application](release_your_application.md)
- [Monitor application performance](../operations/index.md)
- [Analyze GitLab usage](../user/analytics/index.md)
-->