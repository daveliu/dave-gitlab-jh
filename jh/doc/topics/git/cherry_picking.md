---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
comments: false
---

# Cherry pick **(FREE)**

在 Git 中，您可以 *cherry-pick* 现有分支的提交（一组更改），并将这些更改应用到另一个分支。Cherry-picks 可以帮助您：

- 从默认分支到先前版本分支的反向移植错误修复。
- 从 fork 复制更改到上游仓库<!--[到上游仓库](../../user/project/merge_requests/cherry_pick_changes.md#cherry-pick-into-a-project)-->。

您可以从命令行挑选提交。在用户界面中，您还可以：

- Cherry-pick 来自合并请求的所有更改<!--[来自合并请求的所有更改](../../user/project/merge_requests/cherry_pick_changes.md#cherry-pick-a-merge-request)-->。
- Cherry-pick 单次提交<!--[单次提交](../../user/project/merge_requests/cherry_pick_changes.md#cherry-pick-a-commit)-->。
- Cherry-pick 从 fork 到上游存储库<!--[从 fork 到上游存储库](../../user/project/merge_requests/cherry_pick_changes.md#cherry-pick-into-a-project)-->。

## 通过命令行 Cherry-pick

以下说明如何从默认分支（`main`）中挑选一个提交到不同的分支（`stable`）：

1. 查看默认分支，然后基于它查看一个新的 `stable` 分支：

   ```shell
   git checkout main
   git checkout -b stable
   ```

1. 改回默认分支：

   ```shell
   git checkout main
   ```

1. 进行更改，然后提交它们：

   ```shell
   git add changed_file.rb
   git commit -m 'Fix bugs in changed_file.rb'
   ```

1. 显示提交日志：

   ```shell
   $ git log

   commit 0000011111222223333344444555556666677777
   Merge: 88888999999 aaaaabbbbbb
   Author: user@example.com
   Date:   Tue Aug 31 21:19:41 2021 +0000
   ```

1. 识别 `commit` 行，并复制该行上的字母和数字字符串。此信息是提交的 SHA（安全哈希算法）。SHA 是此提交的唯一标识符，您在以后的步骤中需要它。

1. 现在知道了 SHA，再次检查 `stable` 分支：

   ```shell
   git checkout stable
   ```

1. Cherry-pick 提交到 `stable` 分支，并将 `SHA` 更改为你的提交 SHA：

   ```shell
   git cherry-pick <SHA>
   ```

<!--
## Related links

- Cherry-pick commits with [the Commits API](../../api/commits.md#cherry-pick-a-commit)
- Git documentation [for cherry-picks](https://git-scm.com/docs/git-cherry-pick)
-->