---
stage: Growth
group: Conversion
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: howto
---

# 使用许可证激活极狐GitLab **(PREMIUM SELF)**

要启用极狐GitLab 的功能，您需要激活您的实例。激活前，应首先确认运行的版本类型是极狐GitLab。确认版本时，登录 GitLab，并在浏览地址后添加 `/help`，版本类型（edition）和版本号（version）列在 **Help** 页面。

如果您正在运行 GitLab 社区版（CE，Community Edition），可以升级到极狐GitLab。在升级过程中出现问题或需要协助时，请您[联系技术支持](https://about.gitlab.cn/support/#contact-support)。


<!-- As of GitLab Enterprise Edition 9.4.0, a newly-installed instance without an
uploaded license only has the Free features active. A trial license
activates all Ultimate features, but after
[the trial expires](#what-happens-when-your-license-expires), some functionality
is locked.-->

对于全新安装的极狐GitLab 实例，在未上传许可证的情况下，只激活免费特性。试用许可证可以激活专业版，但在[试用到期](#许可证到期时会发生什么)后，一些功能会被锁定。

## 使用激活码激活极狐GitLab **(PREMIUM SELF)**

从 14.0 开始，您需要一个激活码来激活您的实例。 您可以通过[购买许可证](https://about.gitlab.cn/pricing/) 或注册[免费试用](https://about.gitlab.cn/free-trial)来获取激活码。此激活码是您在确认电子邮件中收到的 24 个字符的字母数字字符串。<!--You can also sign in to the [Customers Portal](https://customers.gitlab.com/customers/sign_in) to copy the activation code to your clipboard.-->

要使用您的激活码开始激活过程：

1. 登录您的自助管理实例。
1. 在顶部菜单，选择 **{admin}**。
1. 在左侧导航栏，选择 **订阅**。
1. 将激活码粘贴到输入字段中。
1. 阅读并接受服务条款。
1. 选择 **激活**。

## 使用许可证文件激活极狐GitLab **(PREMIUM SELF)**

如果您收到许可证文件（例如新试用版），您可以通过以管理员身份登录到您的 GitLab 实例或在安装过程中添加它来上传它。 许可证是一个 base64 编码的 ASCII 文本文件，扩展名为“.gitlab-license”。


## 上传您的许可证

<!--The first time you visit your GitLab EE installation signed in as an administrator,
you should see a note urging you to upload a license with a link that takes you
to **Admin Area > License**.

Otherwise, you can:-->

使用管理员账户首次登录访问极狐GitLab 时，您将看到提示上传许可证的信息，点击链接可跳转至**许可证**区域进行操作。

除此之外，您还可以手动跳转至**许可证**区域：

<!--1. Navigate manually to the **Admin Area** by selecting the wrench (**{admin}**) icon in the top menu.-->

1. 登录到您的自助管理实例。

1. 在顶部菜单栏，选择 **目录 >** **{admin}** **管理**。

2. 在左侧导航栏，选择 **订阅**，然后选择 **上传新许可证**。

	* *如果您已收到一个 `.gitlab-license` 文件：*
		1. 下载许可证文件到您的本地机器。
		2. 点击 **Upload `.gitlab-license` file**。
		3. 点击 **选择文件** 并选择许可证文件。在本例中，license 文件名称为 `GitLab.gitlab-license`。
		4. 点击 **同意遵守服务条款** 选择框。
		5. 点击 **Upload License**。

		![Upload license](img/license_upload_v13_12.png)

	* *如果您已收到一个许可证文本密钥：*
		1. 点击 **Enter license key**。
		2. 复制许可证文本并粘贴到 **License key** 区域。
		3. 点击 **同意遵守服务条款** 选择框。
		4. 点击 **Upload License**。

## 安装过程中添加许可证

<!--A license can be automatically imported at install time by placing a file named
`Gitlab.gitlab-license` in `/etc/gitlab/` for Omnibus GitLab, or `config/` for source installations.

You can also specify a custom location and filename for the license:-->

许可证可以在安装过程中自动导入，需要将许可证文件命名为 `Gitlab.gitlab-license`，通过 Omnibus GitLab 安装时放置在 `/etc/gitlab/` 路径下，通过源安装时放置在 `config/` 路径下。

您也可以为许可证指定自定义的位置和文件名称：

<!--- Source installations should set the `GITLAB_LICENSE_FILE` environment
  variable with the path to a valid GitLab Enterprise Edition license.-->

* 通过源安装时，应当设置 `GITLAB_LICENSE_FILE` 环境变量，值为有效的极狐GitLab 许可证的路径。 

  ```shell
  export GITLAB_LICENSE_FILE="/path/to/license/file"
  ```

<!--- Omnibus GitLab installations should add this entry to `gitlab.rb`:-->

* 通过 Omnibus GitLab 安装时，应当向 `gitlab.rb` 添加以下内容。

  ```ruby
  gitlab_rails['initial_license_file'] = "/path/to/license/file"
  ```

<!--WARNING:
These methods only add a许可证at the time of installation. Use the
**{admin}** **Admin Area** in the web user interface to renew or upgrade licenses.-->

WARNING:
这些方法只适用于在安装过程中添加一个许可证。在 web 用户界面中使用 **{admin}** **管理中心** 方可续订或更新许可证。

---

<!--After the license is uploaded, all GitLab Enterprise Edition functionality
is active until the end of the许可证period. When that period ends, the
instance will [fall back](#what-happens-when-your-license-expires) to Free-only
functionality.

You can review the license details at any time by going to **Admin Area > License**.-->

上传许可证后，所有极狐GitLab 功能将被激活，直到许可证的有效期结束。当有效期结束时，实例将[回退](#许可证到期时会发生什么)到只提供免费功能。

您可以在任何时候访问 **管理中心 > 订阅**，查看许可证的详细信息。

## 许可证到期前通知

<!--One month before the许可证expires, a message informing about the expiration
date is displayed to GitLab administrators. Make sure that you update your
license, otherwise you miss all the paid features if your许可证expires.-->

在许可证到期前一个月，系统将向管理员展示关于到期日的一条信息。确保更新您的许可证，否则在许可证到期时，您将不能使用所有的付费特性。

## 许可证到期时会发生什么

<!--When your license expires, GitLab locks down features, like Git pushes
and issue creation. Then, your instance becomes read-only and
an expiration message is displayed to all administrators.

For GitLab self-managed instances, you have a 14-day grace period
before this occurs.

- To resume functionality, upload a new license.
- To fall back to Free features, delete the expired license.-->

当您的许可证到期时，系统将锁定例如 Git 推送和 issue 创建等功能，之后，您的实例变为只读，并向所有管理员展示一条到期信息。

对于自助管理实例，您有 14 天宽限期避免以上情况发生。

* 如果要保留功能可用，上传一个新的许可证。
* 如果要回退到使用免费特性，删除所有到期的许可证。

### 删除许可证文件

<!--To remove a license from a self-managed instance:

1. In the top navigation bar, click the **{admin}** wrench icon to navigate to the [Admin Area](index.md).
1. Click **License** in the left sidebar.
1. Click **Remove License**.-->

从自助管理实例中删除许可证文件的操作如下：

1. 在顶部导航栏，选择 **目录 >** **{admin}** **管理** 跳转到[管理中心](index.md)。
2. 在左侧导航栏，点击 **订阅**。
3. 选择 **删除许可证**。

可能需要重复这些步骤以完全删除所有许可证，包括过去应用的许可证。

## 许可证历史

<!--You can upload and view more than one license, but only the latest许可证in the current date
range is used as the active license. When you upload a future-dated license, it
doesn't take effect until its applicable date.

NOTE:
In GitLab 13.6 and earlier, a notification banner about an expiring license may continue to be displayed even after a new许可证has been uploaded.
This happens when the newly uploaded license's start date is in the future and the expiring one is still active.
The banner disappears after the new license becomes active.-->

您可以上传、查看多个许可证，但只有当前日期范围内的最新许可证，可以用作有效的许可证。

当您上传未来日期的许可证时，它在其适用日期之前不会生效。您可以在 **订阅历史记录** 表中查看所有活动订阅。

您可以在 **订阅历史** 表中查看所有激活订阅。

## 故障排查

### 管理中心没有许可证选项

<!--If you originally installed Community Edition rather than Enterprise Edition you must
[upgrade to Enterprise Edition](../../update/index.md#community-to-enterprise-edition)
before uploading your license.

GitLab.com users can't upload and use a self-managed license. If you
want to use paid features on GitLab.com, you can
[purchase a separate subscription](../../subscriptions/gitlab_com/index.md).-->

如果您原先安装的是社区版（Community Edition）而不是极狐GitLab，您必须在上传许可证前升级到极狐GitLab。

<!--
Gitlab.cn 用户无法上传、使用一个适用于自助管理版的 license。如果您希望在 Gitlab.cn 上使用付费特性，您可以[订阅极狐GitLab SaaS 版](../../subscriptions/gitlab_com/index.md)。
-->

### 续订时用户数超过许可证限制

<!--If you've added new users to your GitLab instance prior to renewal, you may need to
purchase additional seats to cover those users. If this is the case, and a license
without enough users is uploaded, GitLab displays a message prompting you to purchase
additional users. More information on how to determine the required number of users
and how to add additional seats can be found in the
[licensing FAQ](https://about.gitlab.com/pricing/licensing-faq/).-->

如果您在续期前为您的许可证实例添加了新的用户，您可能需要为这些用户购买额外的配额。在此情况下，当上传了一个不能满足用户数量的许可证时，系统将显示一条信息，提醒您购买额外的用户数配额。<!--关于如何确定需要的用户数配额，以及如何添加额外配额，请参考 [licensing FAQ](https://about.gitlab.cn/pricing/licensing-faq/)。-->

在 14.2 及更高版本中，对于使用许可证文件的实例，您可以超过购买的用户数量并仍然激活您的许可证。

- 如果超过订阅的用户少于或等于 10%，则应用许可并在下一次校准时支付超额部分。
- 如果超过订阅用户的10%，则在不购买更多用户的情况下无法申请许可证。

例如，如果您购买了 100 个用户的许可证，则激活许可证时您可以拥有 110 个用户。 但是，如果您有 111，则必须购买更多用户才能激活。


### 存在一个连接问题

在 14.0 及更高版本中，要激活您的订阅，您的极狐GitLab 实例必须连接到 Internet。

如果您有离线环境，则可以[上传许可证文件](#上传您的许可证)代替。

如果您在激活实例时有任何疑问或需要帮助，请[联系技术支持](https://about.gitlab.cn/support/#contact-support)。

