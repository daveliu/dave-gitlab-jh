# frozen_string_literal: true

require 'spec_helper'

RSpec.describe BillingPlansHelper do
  describe '#use_new_purchase_flow?' do
    it 'returns false in JiHu' do
      expect(helper.use_new_purchase_flow?(1)).to be_falsey
    end
  end

  describe '#plan_purchase_or_upgrade_url' do
    it 'returns JiHu common purchase url' do
      expect(helper.plan_purchase_or_upgrade_url(1, 2)).to eq('https://about.gitlab.cn/upgrade-plan')
    end
  end
end
