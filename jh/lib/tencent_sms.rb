# frozen_string_literal: true

require 'tencentcloud-sdk-common'
require 'tencentcloud-sdk-sms'

module TencentSms
  include TencentCloud::Common
  include TencentCloud::Sms::V20210111
  def self.send_code(phone)
    cre = Credential.new(ENV['TC_ID'], ENV['TC_KEY'])
    cli = Client.new(cre, 'ap-beijing')

    random_code = (SecureRandom.random_number(9e5) + 1e5).to_i.to_s

    phonenumberset = ["+86#{phone}"]
    smssdkappid = ENV['TC_APP_ID'] || "1400551828"
    templateid = ENV['TC_TEMPLATE_ID'] || "1052297"
    signname = ENV['TC_SIGN_NAME'] || "极狐GitLab"
    templateparamset = [random_code, "10"]
    extendcode = nil
    sessioncontext = nil
    senderid = nil

    req = SendSmsRequest.new(
      phonenumberset,
      smssdkappid,
      templateid,
      signname,
      templateparamset,
      extendcode,
      sessioncontext,
      senderid
    )
    cli.SendSms(req)
    random_code
  rescue TencentCloudSDKException => e
    Gitlab::AppLogger.error(e)
    nil
  end
end
