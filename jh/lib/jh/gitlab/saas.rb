# frozen_string_literal: true

module JH
  module Gitlab
    module Saas
      extend ActiveSupport::Concern

      class_methods do
        extend ::Gitlab::Utils::Override

        override :com_url
        def com_url
          'https://gitlab.cn'
        end

        override :staging_com_url
        def staging_com_url
          'https://staging.gitlab.cn'
        end

        override :subdomain_regex
        def subdomain_regex
          %r{\Ahttps://[a-z0-9]+\.gitlab\.cn\z}.freeze
        end

        override :dev_url
        def dev_url
          'https://dev-ops.gitlab.cn'
        end

        override :registry_prefix
        def registry_prefix
          'gitlab-jh-public.tencentcloudcr.com'
        end

        override :customer_support_url
        def customer_support_url
          'https://support.gitlab.cn'
        end

        override :customer_license_support_url
        def customer_license_support_url
          'https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293'
        end

        override :gitlab_com_status_url
        def gitlab_com_status_url
          'https://status.gitlab.cn'
        end

        def commom_purchase_url
          'https://about.gitlab.cn/upgrade-plan'
        end
      end
    end
  end
end
